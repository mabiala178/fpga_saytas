`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: KOCAEL� UN�
// Engineer: BURAK YEN�AYDIN
// 
// Create Date: 12/26/2021 10:32:07 PM
// Design Name: 
// Module Name: I2C_TOP
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module I2C_TOP(

input clk,    // 125 MHz , board clock kaynagi
input rst_n,  // sw 0
inout sda,    //PMOD
inout scl     // PMOD

    );
    
    parameter CLKFREQ = 125_000_000;
    parameter I2C_BUS_CLK = 400_000;
    

     
 // OLED modulunu diger moduller ile baglantilarini saglayacak 
    integer OLED_DEVICE_ADDR =  7'b0111100;   // OLED in adres�
    wire  OLED_trig_i2c_driver;
    wire  busy;
    wire  [7:0] OLED_data_wr;
    reg OLED_rw = 0;
    
    wire [9:0] ADDRA;
    wire [7:0] DOUTA;
    wire [3:0] s_state;
    wire OLED_INTERRUPT;
    reg OLED_selection = 0; 
    
    
    // DS3231 modulunu diger moduller ile baglantilarini saglayacak 
    
    integer DS3231_DEVICE_ADDR =  7'b1101000;
    wire  DS3231_trig_i2c_driver;
    
    wire  DS3231_rw;
    wire  [7:0] DS3231_data_wr;
    
    wire [9:0] TEMP;
    wire DS3231_INTERRUPT;// = 0;
    reg DS3231_selection; 
    
    
    // COMMON SIGNAL
    reg [6:0] DEVICE_ADDR;
    reg trig_i2c_driver;
    reg rw;
    reg [7:0] data_wr;
    wire [7:0] data_rd;
    
    reg [1:0] selection_state;
    localparam SELECT_OLED = 0, WAIT1 = 1, SELECT_DS3231 = 2, WAIT2 =3;
    
    reg [15:0] wait_cnt;
    
    // instantiate
    ds3231 ds3231 (
          .clk(clk),    
          .rst_n(rst_n),     
          .interrupt(DS3231_INTERRUPT),      
          .temp(TEMP),  
          .ena(DS3231_trig_i2c_driver),    
          .rw(DS3231_rw) ,
          .data_wr(DS3231_data_wr) , 
           .busy(busy) , 
           .data_rd(data_rd)  ,
           .DS3231_selection(DS3231_selection)  
       );  
       
       

      
    
RAM RAM_i (
      .clka(clk),    
      .addra(ADDRA),      
      .dina(8'h00),      
      .douta(DOUTA),  
      .ena(1),    
      .wea(0)  
   );    
   
   
      OLED OLED_i ( //SSD1306
         .PDO(OLED_data_wr),    
         .MADDR(ADDRA),     
         .MemDI(DOUTA),      
         .clk(clk),  
         .rst_n(rst_n),    
         .Bsy(busy) ,
         .Trig(OLED_trig_i2c_driver) , 
         .Interrupt(OLED_INTERRUPT) , 
         .i_state(s_state)  ,
         .OLED_selection(OLED_selection)  ,
         .Temp_enable(DS3231_INTERRUPT) ,
         .Temp_value(TEMP)   
      );  
      
         
         i2c_driver i2c_driver (
            .clk(clk),    
            .reset_n(rst_n),     
            .ena(trig_i2c_driver),      
            .addr(DEVICE_ADDR),  
            .rw(rw),    
            .data_wr(data_wr) ,
            .busy(busy) , 
            .data_rd(data_rd) , 
            .ack_error()  ,
            .sda(sda)  ,
            .scl(scl) ,
            .o_state(s_state)   
         );  
      
      
      
      always@(*) begin 
      
      
      // bu prosedurun icerisinde secilen state in durumuna gore yapilacak parametre depgisiklikleri yer almaktadir.
            
        
    // DEVICE ADDRESS atamas� hangi slave surulecekse onun adresi
        if (selection_state == SELECT_OLED) begin
          DEVICE_ADDR <= OLED_DEVICE_ADDR;
        end else if (selection_state == SELECT_DS3231) begin 
          DEVICE_ADDR <= DS3231_DEVICE_ADDR;
        end else begin
          DEVICE_ADDR <= 0;
        end
      
// hangi slave in tetiklenece�ini ayar� 
        if (selection_state == SELECT_OLED) begin
          trig_i2c_driver <= OLED_trig_i2c_driver;
        end else if (selection_state == SELECT_DS3231) begin 
          trig_i2c_driver <= DS3231_trig_i2c_driver;
        end else begin
          trig_i2c_driver <= 0;
        end        
   
   // hangi rwbilgisi kullanilaak 
        if (selection_state == SELECT_OLED) begin
          rw <= OLED_rw;
        end else if (selection_state == SELECT_DS3231) begin 
          rw <= DS3231_rw;
        end else begin
          rw <= 0;
        end    
// hangi data yazilacak        
        if (selection_state == SELECT_OLED) begin
          data_wr <= OLED_data_wr;
        end else if (selection_state == SELECT_DS3231) begin 
          data_wr <= DS3231_data_wr;
        end else begin
          data_wr <= 0;
        end               
     end
     
     always @(posedge clk, negedge rst_n) begin   // saatin y�kselen kenar� veya reset geldi�inde
     
     // Ba�langicta sicaklik bilgisi okunacak
     
       if (rst_n ==0) begin
         selection_state  <= SELECT_DS3231; //, SELECT_OLED, SELECT_DS3231)
         OLED_selection   <= 0;
         DS3231_selection <= 0;
         wait_cnt         <= 0;  
       end else begin 
         OLED_selection   <= 0;
         DS3231_selection <= 0;        
         
         case(selection_state)
           
           SELECT_DS3231: begin
             if(DS3231_INTERRUPT == 1) begin   // sicaklik okuma islemi bitirilmis demektir
               selection_state <= WAIT1;
             end
             DS3231_selection <= 1;
           end// SELECT_DS3231
           
           WAIT1: begin
             wait_cnt <= wait_cnt + 1;
             if(wait_cnt[15] == 1) begin   // sicaklik okunduktan sonra bir sure bekle
               wait_cnt        <= 0;
               selection_state <= SELECT_OLED;   // sure doldugunda OLED state ine git
             end
           end// WAIT1
           
           SELECT_OLED: begin
             if(OLED_INTERRUPT == 1) begin   // OLED yazma islemini bitirdikten sonra
               selection_state <= WAIT2;
             end
             OLED_selection <= 1;
           end// SELECT_OLED
  
           WAIT2: begin
             wait_cnt <= wait_cnt + 1;
             if(wait_cnt[15] == 1) begin   // yine bir sure bekle
               wait_cnt        <= 0;
               selection_state <= SELECT_DS3231;  // tekrar sicaklik oku
             end
           end// WAIT2         
           
         default: 
           selection_state <= SELECT_DS3231;
         
         endcase
       end
     
     end 
   
   
   
endmodule
