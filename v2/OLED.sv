`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:  Kocaeli universitesi
// Engineer: Burak Yeniayd�n
// 
// Create Date: 10/31/2021 08:47:18 PM
// Design Name: 
// Module Name: OLED
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OLED(

    input clk,               // 125 Mhz
    input rst_n,             // 0 oldu�unda reset al�r
    input Bsy,               // i2c_driver�n m�satilik durumu
	
    input [7:0] MemDI,       // Block RAM'den gelen data
    output reg [9:0] MADDR,  // Block RAM'in 1024 adresi var
	
    output reg [7:0] PDO,    // i2c_driver�na g�nderilecek veri
	
    output reg Trig,         // i2c_driver�n� tetikleyen sinyal   - ena ya ba�l� top mod�lde
    output reg Interrupt,    // OLED �evriminin bitti�ini belirtir 
	
    input [3:0] i_state,     // i2c_driver�n�n state'i 
    input OLED_selection,    // I2C ba�lant�s�n�n i�in s�ran�n OLED oldu�unu belirtir.
	
    input Temp_enable,       // S�cakl�k de�erinin haz�r oldu�unu belirtir
    input [9:0] Temp_value   // S�cakl�k de�eri

    );

    reg [9:0] MemAddr;

   // 3 farkl� state kullan�ld�
   
   // 1 - OLED konfig�rasyon
   // 2 - OLED alan�n�n ayarlanmas�
   // 3 - OLED'e data g�nderimi


    localparam [1:0] IDLE                   = 0, // OLED Konfigurasyon State leri
                     SEND_CONFIG_DATA       = 1,
                     WAIT_FOR_CONFIG_DATA   = 2,
                     WAIT_FOR_NEW_CMD_TRANS = 3;

    reg [1:0] conf_state; 



    localparam [2:0] INIT                  = 0, // OLED mesaj g�nderin �ncesi konfig�rasyon state leri
                     SEND_CONTROL_DATA     = 1,
                     WAIT_FOR_CONTROL_DATA = 2,
                     WAIT_FOR_NEW_TRANS    = 3,
					 WAIT_FOR_A_WHILE      = 4,
					 RESET1                = 5,
					 RESET2                = 6,
					 WAIT_AFTER_RESET      = 7;

    reg [2:0] data_conf_state; 
	
	
	
    localparam [2:0] INIT_2                  = 0, // OLED'e data g�nderim state'leri
                     SEND_CONTROL_DATA_2     = 1,
                     WAIT_FOR_CONTROL_DATA_2 = 2,
                     WAIT_FOR_NEW_TRANS_2    = 3,
					 WAIT_FOR_A_WHILE_2      = 4;

    reg [2:0] send_data_state; 


// ################# OLED konfig�rasyonu i�in gereken de�erler #################
    localparam [7:0] cmd_set_display_off               = 8'hAE;

    localparam [7:0] cmd_set_osc_freq                  = 8'hD5;
    localparam [7:0] val_set_osc_freq                  = 8'h80;

    localparam [7:0] cmd_set_mux_ratio                 = 8'hA8;
    localparam [7:0] val_set_mux_ratio                 = 8'h3F;

    localparam [7:0] cmd_set_display_offset            = 8'hD3;
    localparam [7:0] val_set_display_offset            = 8'h00;

    localparam [7:0] cmd_set_display_start_line        = 8'h40;

    localparam [7:0] cmd_set_enable_charge_pumup_reg   = 8'h8D;
    localparam [7:0] val_set_enable_charge_pumup_reg   = 8'h14;

    localparam [7:0] cmd_set_memory_addr_mode          = 8'h20;
    localparam [7:0] val_set_memory_addr_mode          = 8'h00;

    localparam [7:0] cmd_set_segment_remap             = 8'hA1;

    localparam [7:0] cmd_set_com_out_scan_direct       = 8'hC8;

    localparam [7:0] cmd_set_com_pins_hw_config        = 8'hDA;
    localparam [7:0] val_set_com_pins_hw_config        = 8'h12;

    localparam [7:0] cmd_set_contrast_ctrl             = 8'h81;
    localparam [7:0] val_set_contrast_ctrl             = 8'hCF;

    localparam [7:0] cmd_set_precharge_period          = 8'hD9;
    localparam [7:0] val_set_precharge_period          = 8'hF1;

    localparam [7:0] cmd_set_v_comh_deselect_level     = 8'hDB;
    localparam [7:0] val_set_v_comh_deselect_level     = 8'h40;

    localparam [7:0] cmd_set_disable_entire_display_on = 8'hA4;

    localparam [7:0] cmd_set_normal_display            = 8'hA6;

    localparam [7:0] cmd_set_display_on                = 8'hAF;
// ####################################################################


    localparam number_config_param = 25; //toplam konfig�rasyon say�s�

    integer conf_index;
    integer data_index;



// Konfigurasyonlar� bir dizi haline getirdik. 
// Asl�nda bir tane daha BRAM kullanarak bunu yapabilirdik.
    localparam [7:0] config_param [0:(number_config_param - 1)] = { //25 sat�rl�k 8 s�tundan olu�an 2D array 8 er bit okunuyor
      cmd_set_display_off,
      cmd_set_osc_freq,                 
      val_set_osc_freq,                                     
      cmd_set_mux_ratio,                
      val_set_mux_ratio,                                                  
      cmd_set_display_offset,           
      val_set_display_offset,                                             
      cmd_set_display_start_line,                                         
      cmd_set_enable_charge_pumup_reg,  
      val_set_enable_charge_pumup_reg,                                   
      cmd_set_memory_addr_mode,  
      val_set_memory_addr_mode,                                   
      cmd_set_segment_remap,                                   
      cmd_set_com_out_scan_direct,                                   
      cmd_set_com_pins_hw_config,  
      val_set_com_pins_hw_config,                                   
      cmd_set_contrast_ctrl,  
      val_set_contrast_ctrl,                                   
      cmd_set_precharge_period,  
      val_set_precharge_period,                                  
      cmd_set_v_comh_deselect_level,  
      val_set_v_comh_deselect_level,                                  
      cmd_set_disable_entire_display_on,                                
      cmd_set_normal_display,                                           
      cmd_set_display_on
      };


    reg [7:0] OLED_display_area_range [0:5] = {8'h21, 8'h00, 8'h7F, 8'h22, 8'h00, 8'h07};   // ba�langic degeri olarak olarak t�m pixeller secili

    reg is_display_config_finish;  
    reg is_display_data_cmd_finish;
    reg [15:0] data_cmd_cnt;
    reg is_display_data_cmd_enabled;
	
	
	reg is_reset_done;
	reg [19:0] wait_cnt;

	integer digit_index ;
	
	localparam [7:0] column_base_index = 8'h1D;     // OLED'e yazmaya baslayacagimiz sutun sayisi hex olarak 1d 
	integer column_base_incr           = 14;        // her bir karakterim 24 sat�r 14 sutundan olusugu icin her bir karatakterde 14 pixel kaydir
 
	localparam [7:0] row_base_start_index = 8'h03;   // OLED'e yazmaya baslayacagimiz page sayisi hex olarak 03   -- page hep sabit degismeyecek
	localparam [7:0] row_base_stop_index  = 8'h05;   // OLED'e yazmay� bitirecegimiz page sayisi hex olarak 05 
 
 	reg [7:0] current_column_start_index;            // mevcut sutun bilgileri
 	reg [7:0] current_column_stop_index;
 
 	reg [7:0] current_row_start_index;                // mevcut page bilgileri 
 	reg [7:0] current_row_stop_index;
 
 	reg [9:0] temp_value_hex;
 	integer temp_value_int;
 	integer first_digit;          // tam kismin ilk basamagi
 	integer second_digit;         // tam kismin ikinci basamagi
 
 	integer first_float_part;     // ondalik kismin ilk basamagi
 	integer second_float_part;    // ondalik kismin ikinci basamagi

	reg [31:0] temp_array [0:4];   // tum basamakar bu arrayde tutuluyor


always@(posedge clk, negedge rst_n)  //Bu always blo�u Temp_enable = 1 oldu�unda Temp_value de�erini okumam�z� sa�lar
begin
  if(!rst_n) begin
    temp_value_hex <= 0;
  end else begin
    if(Temp_enable) begin
	  temp_value_hex <= Temp_value;   // temp enable geldi�i zaman y�kselen kenarda DS3231 den gelen temp value yu oku
	end  
  end
end  


assign temp_value_int = temp_value_hex[9:2];  //S�cakl�k de�erini tam k�sm�n� al�yoruz

assign first_digit    = (temp_value_int / 10); //S�cakl�k de�eri basamaklar�na ayr�l�yor
assign second_digit   = (temp_value_int % 10); //S�cakl�k de�eri basamaklar�na ayr�l�yor


// Ondal�k k�s�mlar iki bit halinde gelir ve biz bu bitler yorumlayarak donusumu saglamaliyiz.
// 00 01 10 11 - 00 25 50 75 donusumu

//Bu combinitial always blo�u s�cakl�k verisinin ilk ondal�k k�sm�n�n de�erini belirlemek i�in kullan�l�r
always@(*) begin  // i�erideki de�erlerden herhangi birinde bir de�i�im oldu�unda bu always blo�u �al���r 
  if(temp_value_hex[1:0] == 2'b00) begin 
    first_float_part = 0;
  end else if(temp_value_hex[1:0] == 2'b01) begin
    first_float_part = 2;
  end else if(temp_value_hex[1:0] == 2'b10) begin
    first_float_part = 5;
  end else begin
    first_float_part = 7;
  end 
end 

//Bu combinitial always blo�u s�cakl�k verisinin ikinci ondal�k k�sm�n�n de�erini belirlemek i�in kullan�l�r
always@(*) begin  // i�erideki de�erlerden herhangi birinde bir de�i�im oldu�unda bu always blo�u �al���r 
  if(temp_value_hex[1:0] == 2'b00) begin 
    second_float_part = 0;
  end else if(temp_value_hex[1:0] == 2'b01) begin
    second_float_part = 5;
  end else if(temp_value_hex[1:0] == 2'b10) begin
    second_float_part = 0;
  end else begin
    second_float_part = 5;
  end 
end 

assign temp_array[0] = first_digit;        // Elde edilen s�cakl�k de�erin en de�erlikli basama��n� RAM'in addresine atamak i�in
assign temp_array[1] = second_digit;       // Elde edilen s�cakl�k de�erin 2. en de�erlikli basama��n� RAM'in addresine atamak i�in
assign temp_array[2] = 10;                 // "." (Nokta) Blok RAM'in 10. karakteri
assign temp_array[3] = first_float_part;   // Elde edilen s�cakl�k de�erin ondal�k basama��ndaki en de�erlikli basama��n� RAM'in addresine atamak i�in
assign temp_array[4] = second_float_part;  // Elde edilen s�cakl�k de�erin ondal�k basama��ndaki 2. en de�erlikli basama��n� RAM'in addresine atamak i�in



// Burada OLED in hangi piksellerine deger yaz�lacag�n� belirtecek olan sat�r ve sutun baslang�c ve bitis indekslerini ayarliyoruz

//Bu combinitial always blo�u OLED'in hangi s�tununa yaz�lmaya ba�lanaca��n� belirtmek i�in kullan�l�r
always@(*) begin  // i�erideki de�erlerden herhangi birinde bir de�i�im oldu�unda bu always blo�u �al���r 
  if(is_reset_done == 1) begin // E�er OLED'in t�m pixellerine 0 yaz�lmad�ysa. Bu normal operasyon durumu
    current_column_start_index = (column_base_index + digit_index * column_base_incr);
  end else begin               //OLED ekran�n temizlemek i�in t�m pixelleri se�
    current_column_start_index = OLED_display_area_range[1]; //8'h00
  end 
end 

//Bu combinitial always blo�u OLED'in hangi s�tununa kadar yaz�laca��n� belirtmek i�in kullan�l�r
always@(*) begin  // i�erideki de�erlerden herhangi birinde bir de�i�im oldu�unda bu always blo�u �al���r 
  if(is_reset_done == 1) begin // E�er OLED'in t�m pixellerine 0 yaz�lmad�ysa. Bu normal operasyon durumu
    current_column_stop_index = (column_base_index + (digit_index+1) * column_base_incr - 1);
  end else begin               //OLED ekran�n temizlemek i�in t�m pixelleri se�
    current_column_stop_index = OLED_display_area_range[2];// 8'h7F
  end 
end 

//Bu combinitial always blo�u OLED'in hangi sat�r�ndan yaz�lmaya ba�lanaca��n� belirtmek i�in kullan�l�r
always@(*) begin  // i�erideki de�erlerden herhangi birinde bir de�i�im oldu�unda bu always blo�u �al���r 
  if(is_reset_done == 1) begin // E�er OLED'in t�m pixellerine 0 yaz�lmad�ysa. Bu normal operasyon durumu
    current_row_start_index = (row_base_start_index);
  end else begin               //OLED ekran�n temizlemek i�in t�m pixelleri se�
    current_row_start_index = OLED_display_area_range[4]; //8'h00
  end 
end 

//Bu combinitial always blo�u OLED'in hangi s�tununa kadar yaz�laca��n� belirtmek i�in kullan�l�r
always@(*) begin  // i�erideki de�erlerden herhangi birinde bir de�i�im oldu�unda bu always blo�u �al���r 
  if(is_reset_done == 1) begin // E�er OLED'in t�m pixellerine 0 yaz�lmad�ysa. Bu normal operasyon durumu
    current_row_stop_index = (row_base_stop_index);
  end else begin               //OLED ekran�n temizlemek i�in t�m pixelleri se�
    current_row_stop_index = OLED_display_area_range[5];// 8'h07
  end 
end 









always@(posedge clk, negedge rst_n)
begin
  if(!rst_n) begin   // reset n 0 ise buray� yap. 
    conf_state                 <= IDLE;
    conf_index                 <= 0;
    Trig                       <= 0;
    is_display_config_finish   <= 0;
    is_reset_done              <= 0;
    
    data_conf_state            <= INIT;
    data_index                 <= 0;
    is_display_data_cmd_finish <= 0;
    
    send_data_state            <= INIT_2;
    MemAddr                    <= 0;
    wait_cnt                   <= 0;
    OLED_display_area_range    <= {8'h21, 8'h00, 8'h7F, 8'h2, 8'h00, 8'h07}; 
    digit_index                <= 0;
    
    Interrupt                  <= 0;
  end else begin        // else if dedi�miz i�in reset �ncelikli. ilk reset e bak�l�r yoksa clk
    if(OLED_selection == 1) begin   // top modulde secildiyse
      Interrupt                <= 0;   // 1 olunca top module islem bitti art�k s�cakl�k oku demek
      
      
      
      
      //1. state machine    
         // 1 - OLED konfig�rasyon
         
         
      if(is_display_config_finish  == 0) begin
        case(conf_state)     
          IDLE : begin
		    conf_state <= IDLE;
            Trig       <= 1;
            PDO        <= 8'h00; // CONTROL_BYTE
            
            if(Bsy == 1) begin // i2c driver mesgul degilse
              conf_state <= SEND_CONFIG_DATA;
            end           
          end

          SEND_CONFIG_DATA : begin
            conf_state <= SEND_CONFIG_DATA;
            Trig       <= 1;
            PDO        <= config_param[conf_index];
            
            if(i_state == 4'h6) begin // i2c driver SLV_ACK2
              conf_state <= WAIT_FOR_CONFIG_DATA;
              conf_index <= conf_index + 1;
            end                  
          end          

          WAIT_FOR_CONFIG_DATA : begin
            conf_state <= WAIT_FOR_CONFIG_DATA;
            Trig       <= 1;
            if(i_state == 4'h4) begin // i2c driver SLV_ACK2
              Trig       <= 0;
              conf_state <= WAIT_FOR_NEW_CMD_TRANS;
            end                 
          end           

          WAIT_FOR_NEW_CMD_TRANS : begin
            conf_state <= WAIT_FOR_NEW_CMD_TRANS;
            if(i_state == 4'h0) begin
              conf_state <= IDLE;
            end
            if(conf_index == 25) begin //25 config komutu
              is_display_config_finish <= 1;
              conf_index               <= 0;
              conf_state               <= IDLE;                    
            end                 
          end
          
          default : begin
            conf_state <= IDLE;
          end          
        endcase
      end //is_display_config_finish
   
   
   // 2.  state machine
   // 2 - OLED alan�n�n ayarlanmas�
  // 2. ve 3. state machineler pe�i s�ra kullanilmalidir. Her data gonderiminden once OLED in hangi pixeller �zerine yazilacagi belirlenmelidir.
  
      if(is_display_data_cmd_enabled  == 1 && is_display_data_cmd_finish == 0) begin  // bir sat�r s�tun belirle, bir datay� g�nder, s�ra s�ra
        case(data_conf_state)
          INIT : begin
		    data_conf_state <= INIT;
            Trig            <= 1;
            PDO             <= 8'h00; //CONTROL_BYTE    -- command lerden once 00 control byte � gonderilmeli- data dan once 0x40 gonderilmeli
            if(Bsy == 1) begin  //i2c driver mesgul degilse
              data_conf_state <= SEND_CONTROL_DATA;
              OLED_display_area_range <= {8'h21, current_column_start_index , current_column_stop_index, 8'h22, current_row_start_index, current_row_stop_index};
              // Bast�r�lacak alan parametrik olarak bir dizide tutuluyor.
            end          
          end

          SEND_CONTROL_DATA : begin
            data_conf_state <= SEND_CONTROL_DATA;
            Trig            <= 1;
            PDO             <= OLED_display_area_range[data_index];   // yukar�da olusturdugumuz diziyi gonder
            if(i_state == 4'h6) begin // i2c driver SLV_ACK2
              data_conf_state <= WAIT_FOR_CONTROL_DATA;
              data_index      <= data_index + 1;
            end       
          end         
          
          WAIT_FOR_CONTROL_DATA : begin
            data_conf_state <= WAIT_FOR_CONTROL_DATA;
            Trig            <= 1;
            if(i_state == 4'h4) begin // i2c driver SLV_ACK2  -- i2c driver� bu state gelene kadar bekler
              Trig            <= 0;     // ardindan tetiki kald�r�r.
              data_conf_state <= WAIT_FOR_NEW_TRANS;
            end;      
          end          

          WAIT_FOR_NEW_TRANS : begin
            data_conf_state <= WAIT_FOR_NEW_TRANS;
            if(i_state == 4'h0) begin   // i2c driver� 0. state gelene kadar bekler
              data_conf_state <= INIT;  // ard�ndan teni komut gondermek icin INIT stateine dallan
            end
            if(data_index == 6) begin  // OLED yazilacak alani secmek icin yapilacak konf. bitmis demektir.
              data_index      <= 0;
              data_conf_state <= WAIT_FOR_A_WHILE;  //  o halde bir sure bekle
            end 
          end
          
          WAIT_FOR_A_WHILE : begin
            wait_cnt <= wait_cnt + 1;
            if(wait_cnt[15] == 1) begin  // gecikme
              wait_cnt <= 0;
              if(is_reset_done  == 1) begin                   
                is_display_data_cmd_finish <= 1;
                data_conf_state <= INIT;                
              end else begin                   // biraz bekledikten sonra data gonderimi icin 
                data_conf_state  <= RESET1;    // 
                PDO         <= 8'h40;          // yazmak icin kontrol byte ini 0x40 yaptik
                wait_cnt    <= 0;                     
              end
            end      
          end

          RESET1 : begin
            data_conf_state <= RESET1;
            Trig            <= 1;            
            if(i_state == 4'h6) begin //i2c driver SLV_ACK2   -- i2c master 6. state gelene kadar bekle
              data_index <= data_index + 1; 
              PDO             <= 8'h00;      
              data_conf_state <= RESET2;
            end       
          end
          
          RESET2 : begin
            data_conf_state <= RESET2;
            Trig            <= 1;
            if(i_state == 4'h4) begin // i2c driver SLV_ACK2  -- i2c master 4. state gelene kadar bekle
              Trig       <= 0;              // tetiklemeyi b�rak  -- Bu i�lemler coklu data gonderimi icin yapildi
              data_conf_state <= RESET1;
            end                  
            if(data_index == 1025) begin  // b�t�n ekrana 0 yaz  // bir kere giriyor
              data_conf_state  <= WAIT_AFTER_RESET;  // 
              is_reset_done    <= 1;
              Trig             <= 0;
              data_index       <= 0;
            end        
          end 
          
          WAIT_AFTER_RESET : begin
            wait_cnt <= wait_cnt + 1;
            if(wait_cnt[15] == 1) begin  // bekle
              wait_cnt <= 0;
              if(is_reset_done  == 1) begin                  
                OLED_display_area_range <= {8'h21, 8'h00, 8'h7F, 8'h22, 8'h00, 8'h07};  // tum alani sectik
                data_conf_state  <=INIT;
              end else begin
                data_conf_state  <= RESET1;
                PDO         <= 8'h40;                 // t�m alana 0 yazmak icin control byte � ayarla. Yap�lmazsa RAMdeki cop degerler okunur
              end
            end              
          end
          
          default : begin
            data_conf_state <= INIT;
          end                                    
                    
        endcase
        
        
        // 3. state machine
        // 3 - OLED'e data g�nderimi// Bu iki state in beraber kullan�lmas
        
        
      end else if(is_display_data_cmd_finish == 1) begin// is_display_data_cmd_enabled     
        case(send_data_state)
          INIT_2 : begin
            send_data_state <= INIT_2;
            Trig            <= 1;
            PDO             <= 8'h40; // CONTROL_BYTE
            if(Bsy == 1) begin         // i2c driver mesgul degilse
              send_data_state <= SEND_CONTROL_DATA_2;
              MemAddr         <= temp_array[digit_index]*42; // basamk de�eri �arp� 42 -- BLOK RAM deki ilgili basamaign degerini veren alan�n baslagic adresini MemAddr ye ata
              // temp_array[digit_index] igili basamagin degerin sayi degerini verir  23.75 icin ilk index 2 
              // 2* 42 BLOK RAM de 2 karakterinin kayitli oldugu yer
            end
          end
          
          
          SEND_CONTROL_DATA_2 : begin
            send_data_state <= SEND_CONTROL_DATA_2;
            Trig            <= 1;
            PDO             <= MemDI;
            if(i_state == 4'h6) begin // i2c driver SLV_ACK2  -- data yazmaya devam et
              send_data_state <= WAIT_FOR_CONTROL_DATA_2;
              MemAddr         <= MemAddr + 1;               // 8 bit 8 bit yaz�yoruz. Bir sonraki byte a ge�
            end
          end
          
          
          WAIT_FOR_CONTROL_DATA_2 : begin           // Bu state  bir karakter tamamen okunana kadar verileri gonderme islemi yapar.
            send_data_state <= WAIT_FOR_CONTROL_DATA_2;   // Buradaki 4 satir ile 
            Trig            <= 1;
            if(i_state == 4'h4) begin // i2c driver SLV_ACK2  
              send_data_state <= SEND_CONTROL_DATA_2;              
            end
            if(MemAddr == temp_array[digit_index]*42 + 43) begin  // Blok ramde bir karakter okunmu� olur 42 byte
              send_data_state <= WAIT_FOR_A_WHILE_2;
            end 
          end
          
          WAIT_FOR_A_WHILE_2 : begin
            wait_cnt    <= wait_cnt + 1;
            Trig        <= 0;
            if(wait_cnt[19] == 1) begin              // bir karakter yazildiginda biraz bekle ve degerleri 0 layarak baslangic state ine git     
              is_display_data_cmd_finish <= 0;
              send_data_state            <= INIT_2;
              wait_cnt                   <= 0;
              MemAddr                    <= 0;
              Trig                       <= 0;
              
              digit_index <= digit_index + 1;   // bir basamak kaydir
              if(digit_index == 4) begin  // eger 5 basamak yazarsak
                digit_index <= 0;           
                Interrupt   <= 1;    // islem bittti diye top module tetik gonder 
                // bundan sonra sicaklik okunur.
              end
            end  
          end          
          
          default : begin
            send_data_state <= INIT_2;
          end                                     
              
        endcase  
      
      end //is_display_data_cmd_finish
      MADDR <= MemAddr;      
    end // OLED_selection
  end //rst_n'in else
end //always


always@(posedge clk, negedge rst_n) begin
  if(!rst_n) begin    // reset gelirse
    is_display_data_cmd_enabled <= 0;
    data_cmd_cnt                <= 0;   
  end else begin
    if(is_display_config_finish == 1) begin  // OLED konfig bitti ise
      data_cmd_cnt <= data_cmd_cnt + 1;
      if(data_cmd_cnt[15] == 1) begin
        data_cmd_cnt                <= 0;
        is_display_data_cmd_enabled <= 1;
      end
    end    
  end
end

endmodule