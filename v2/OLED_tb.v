`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/27/2021 01:53:00 AM
// Design Name: 
// Module Name: OLED_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OLED_tb(
 // tb de giri� ��k�� tan�mlanmaz
    );
    parameter CLKFREQ = 125_000_000;
    parameter I2C_BUS_CLK = 400_000;
    
    reg CLK	   = 0;
    reg RST_N  = 0;
    wire SCL    = 'bz;
    wire SDA    = 'bz;
    
  I2C_TOP I2C_TOP (
             .clk(CLK),    
             .rst_n(RST_N),     
             .scl(SCL),      
             .sda(SDA)
  
          );  
      
      
   
    
   initial begin     // 20 ns periyotlu clock olu�turdum
        forever begin
        
        #10 CLK = ~CLK;
     end end
     
     
     
     initial  begin
     #100;
     RST_N <= 1;
    end   
    
    
    
    
    
       
          
    
endmodule
