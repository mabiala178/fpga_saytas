`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12/09/2021 07:38:19 PM
// Design Name: 
// Module Name: ds3231
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ds3231(
	
input clk,      
input rst_n,
inout scl,          
inout sda,                  
output reg interrupt,         // i�lemin bitti�ine dair top modulu bilgilendir
output reg [9:0] temp,

output  reg ena,             // i2c driver a tetik
output  reg rw,
output  reg [7:0] data_wr,    // i2c driver a yaz�lacak veriyi g�nder
input   busy,                 // i2c driver me�gul m� de�il mi bilgisi
input   [7:0] data_rd,        // okunan data
input   DS3231_selection      // top modul bu degeri 1 yapmazsa okunmaz
    );
    
    
    

    reg busyPrev = 0;           
    reg [7:0] busyCntr = 0;
    
    reg ack_error = 0;
    reg enable = 0;
    reg waitEn = 0;
    reg [7:0] cntr = 0;  
    

    


    parameter CLKFREQ = 125_000_000;
    parameter I2C_BUS_CLK = 400_000;
    //parameter DEVICE_ADDR = 7'b1101000;
    
    

    localparam IDLE_S=0 , ACQUIRE_S=1;   //state ler 
    reg state = 0;

    integer  cntr250msLim = CLKFREQ/1000  ; // for test  - s�cakl�k de�eri okunduktan sonraaraya uzunca bir OLED e veri yazma s�reci girdi�i i�in problem olmaz
    //integer  cntr250msLim = CLKFREQ/4;   //  4 Hz
    integer cntr250ms = 0 ; // 
    reg cntr250msEn = 1;
    reg cntr250msTick = 0;
    integer is_new_driver_enable=1; 
    
    always@(posedge clk)
    begin
    if(DS3231_selection == 1) begin   // okuma s�ras� bu sens�rdeyse 
    		
    		// IDLE durumunda slave'e register adres icin write command gonderiliyor
            // ilk guc acilip reset kalktiginda 250 ms bekleniliyor
            // bir daha bu 250 ms reset olmadikca beklenmiyor  
              
        case(state)
       
            IDLE_S : begin 
              busyPrev  <= busy;
              if(busyPrev == 0 && busy == 1) begin   // busy de bir y�kselen kenar olu�tuysa
                busyCntr <= busyCntr + 1;         // i2c driver kacinci defa mesgul oldu�u bilgisi 
              end  
              interrupt <= 0;  
              
              if(rst_n == 1) begin              // reset yoksa 250 ms bekle
                ena <=0;
                if(cntr250msTick == 1) begin   // 250 ms sayd�ktan sonra enable � 1 yaparak i2c driver � ba�lat
                  enable <= 1;
                end 
              end else begin
                enable <=0;
              end
                
                
              
                
              if(enable == 1) begin      // Sayma isleminin bitmesi ile tetik verildi ise
                if(busyCntr == 0) begin   // ve i2c driver  me�gul de�ilse
                  ena  <= 1;              // �evrimi ba�lat i2c driver
                  rw   <= 0;           // write
                  data_wr  <= 8'h11;   // s�cakl�k bilgisinin en de�erlikli byte �n register adresi
                end else if(busyCntr == 1) begin   // 1 kere me�gul ise
                  ena <= 0;       // i2cdriver � STOP state ine gonderdik.
                  if(busy ==0) begin   // i2c driver me�gul de�ilse
                    waitEn <= 1;
                    busyCntr <= 0;
                    enable <= 0;
                  end
                end
              end
                
                

      
                
                
              if(waitEn == 1) begin  // stop ile start aras�nda bir s�re bekle write - read aras� tekrar baslatma
                if(cntr == 255) begin
                  state  <= ACQUIRE_S;  //  s�re doldu�unda state i de�i�tir
                  cntr   <= 0;
                  waitEn <= 0;
                end else begin
                  cntr <= cntr + 1;   // say
                end
              
              end  
              
             
            end //IDLE end
            
            ACQUIRE_S : begin
            
                busyPrev   <= busy;
                if(busyPrev == 0 && busy == 1) begin     // yine busy  y�kselen kenar�nda mi bak
                  busyCntr <= busyCntr + 1;              // kac kere busy oldu
                end
                if(busyCntr == 0) begin  // hen�z busy y�kselmemi� veya �u an i2c driver me�gul de�il
                  ena 	<= 1;         // i2c driver a tetik gonder
                  rw     <= 1;        // read
                  data_wr  <= 8'h11;   //  0x11 . register�n� oku 
                end else if(busyCntr == 1) begin    // 1 kere y�kseldi ise
                  if(busy == 0) begin              // busy d��en kenarda ise
                    temp [9:2] <= data_rd;         // okunan veriyi al
                  end
                  rw <= 1;
                end else if(busyCntr == 2) begin // 2 kere yukseldi ise
                  ena <= 0;                
                  if(busy == 0) begin
                    temp [1:0] <= data_rd[7:6];       // 2. register � oku en degerlikli iki bitinde ondal�kl� k�sm�n bilgisi tutuluyor.
                    // 00   01   10  11
                    // .00 .25  .50 .75
                    state <= IDLE_S;
                    busyCntr <= 0;
                    interrupt <= 1;   // i�lemin bitti�ini top mod�le belirtecek
                  end
                end
            end // ACQUIRE_S end
    
        endcase
      end  // if DS3231_selection end  
    end //always end

always@(posedge clk)
    begin
      if(cntr250msEn == 1) begin
        if(cntr250ms == cntr250msLim - 1) begin  // limit de�erine geldiyse 
          cntr250msTick	<= 1;                    //  Saymanin tamamlandigina dair i�aret
          cntr250ms     <= 0;                    //  sayaci 0 ola
        end else begin   // s�rekli say
          cntr250msTick	<= 0;
          cntr250ms     <= cntr250ms  +1;
        end
      end else begin    // sayma islemi aktif degilse
        cntr250msTick	<= 0;
        cntr250ms     <= 0;        
      end
    end // always


endmodule
