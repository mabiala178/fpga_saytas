`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2021 08:47:18 PM
// Design Name: 
// Module Name: i2c_driver
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module i2c_driver(

    input clk,
    input reset_n,           // 0 oldu�unda reset al�r
    input ena,
    input [6:0] addr,        // slave cihaz�n adresi
    input rw,                // 0 ise yazma 1 ise okuma
    input [7:0] data_wr,     // g�nderilecek veri
    output reg busy,             // �al��ma durumu 
    output reg [7:0] data_rd,    // okunan veri  
    output ack_error,        // acknowledge
    inout sda,               // i2c data hatt� 
    inout scl,                // i2c clock hatt�
    output reg [3:0] o_state   // mevcut state bilgisini OLED mod�l�ne g�nderdim (�oklu veri g�nderimi i�in)
    
    );
    parameter integer input_clk = 125_000_000; //input clock speed from user logic in Hz
    parameter integer bus_clk   = 400_000;     //speed the i2c bus (scl) will run at in Hz
    
    localparam [3:0] READY    = 0,    // State ler
                     START    = 1,
                     COMMAND  = 2,
                     SLV_ACK1 = 3,
                     WRITE    = 4,
                     READ     = 5,
                     SLV_ACK2 = 6,
                     MSTR_ACK = 7,
                     STOP     = 8;
    reg [3:0]state;        
    reg data_clk;        // verinin g�nderilece�i clk
    reg data_clk_prev;   // data clk n�n y�kselen kenar�n� bulmak i�in
    reg scl_clk;
    reg scl_ena= 1'b0;
    reg sda_int= 1'b1;     // data hatt� 1 de
    reg sda_ena_n;
    reg [7:0] addr_rw;    
    reg [7:0] data_tx;    
    reg [7:0] data_rx;    
    integer bit_cnt = 7;
    
    reg s_ack_error;
      
    reg stretch = 1'b0; //i2c slave cihazinin mesgul olmasi durumunda master'� durdurmak ister.
                        //Bunun icin slave cihaz streching adi verilen mekanizmayi kullanir. 
                        //Bu mekanizma bu kodda tam olarak gerceklenmemistir.Cunku biz burada sadece scl'yi
                        //surmeyi birakiyoruz. Ancak data surme islemi data_clk ile yapiliyor
    
    localparam divider = (input_clk/bus_clk)/4; //scl ve data_clk aras�nda 
                                                //faz farki olusturmak icin 4 segmentte bolundu
                                                // scl 0 �n ortas�nda iken data g�nderece�iz
    integer count; // scl_clk ve cda_clk clocklarini olusturmak icin kullan�lacakt�r.

always@(posedge clk, negedge reset_n)  
begin
  if(!reset_n) begin
    count   <= 0;
    stretch <= 0;
  end else begin
    data_clk_prev <= data_clk; //rising edge olarak kullanmak icin registerladik    
    
//Bu if bloklari scl saatini olusturmak ve bizim datayi surdugumuz data_clk'u 
//olusturmak icin kullanilmistir. Bu kisim kaldirilark yerine clock wizard kullanilabilir    
    if(count == divider*4-1) begin 
      count   <= 0;
    end else if(stretch == 0) begin
      count   <= count + 1;
    end
    
    if(count >= 0 && count <= divider-1) begin 
      scl_clk  <= 0;  
      data_clk <= 0; 
    end else if(count >= divider && count <= divider*2-1) begin
      scl_clk  <= 0;  
      data_clk <= 1; 
    end else if(count >= divider*2 && count <= divider*3-1) begin
      scl_clk  <= 1;  
      data_clk <= 1;
      
      if(scl == 0) begin //I2C kodlarini inceledi�imde strech mekanizmasinin
        stretch = 1;     // sadece 3/4. period ta kontrol edildigini gordum.
      end else begin     //Bence bunun sebebi yeni data yazilmasindan hemen sonra
        stretch = 0;     //slave taraf�nda hafiza veya farkli bir sebepten kaynakli 
      end                //hatanin olusabilmesi olabilir.
           
    end else begin
       scl_clk  <= 1;  
       data_clk <= 0;     
    end
  end
end  



always@(posedge clk, negedge reset_n)
begin
  if(!reset_n) begin   // reset n 0 ise buray� yap.
    state       <= READY;
    busy        <= 0;
    scl_ena     <= 0;
    sda_int     <= 1;
    s_ack_error <= 0;
    bit_cnt     <= 7;
    data_rd     <= 0;
  end else begin        // else if dedi�miz i�in reset �ncelikli. ilk reset e bak�l�r yoksa clk
    if((data_clk == 1) && (data_clk_prev == 0)) begin  // data_clk y�kselen kenarda ise
    
      case(state)
      
        READY : begin
          if(ena == 1) begin      // �evrimi d��ar�dan ena y� tetikleyerek ba�lat�yoruz
            busy    <= 1;     // art�k me�gul�m
            addr_rw <= {addr, rw};  // concotanate  -- 7 bitlik adres ile 1 bitlik rw yi 8 bitlik addr rw ye ata 
            data_tx <= data_wr;     // d��ar�dan gelen veriyi g�nderim i�in data tx e ata
            state   <= START;  
          end else begin
            busy <= 0;     // ena 1 de�ilse me�gul de�il ve ayn� state de bekle
            state <= READY;
          end
        end  
        
        START : begin
          busy    <= 1;
          sda_int <= addr_rw[bit_cnt];   // addr in en de�erlikli bititnin de�erini sda_int e ata
          state   <= COMMAND;           
        end

        COMMAND : begin
          if(bit_cnt == 0) begin       // adress g�nderilmi� demektir -- aderesle beranber rw biti de g�nderildi
            sda_int <= 1;               // sda_int i  yaparak SDA hatt�n� High Z e �ekiyoruz.Burada asl�nda ack bekliyoruz
            bit_cnt <= 7;               //  slv ack ya ge�meden sda int ve bit cnt ye de�er atad�k.
            state <= SLV_ACK1;  
          end else begin
            bit_cnt <= bit_cnt - 1;          
            sda_int <= addr_rw[bit_cnt-1];   // geriye kalan 7 bitlik addres verisi ve rw bitini sda int e aktard�k  
            state <= COMMAND;
          end
        end
        
        SLV_ACK1 : begin                   
          if(addr_rw[0] == 0) begin        // rw bitinin  0 sa -- yani bir �ey yaz�lacak demek.
            sda_int <= data_tx[bit_cnt];   // g�nderilecek datan�n en de�erlikli bitini sda int e aktar (i2c en de�erlikli bitten ba�layarak g�nderilir.
                        state <= WRITE;     // yani en de�erlikli bitten g�ndermeye ba�lar
          end else begin
            sda_int <= 1;           // rw 1 read
            state <= READ;          // e�er rw 1 ise READ state ine ge�
          end
        end        

        WRITE : begin
          busy <= 1; 
          if(bit_cnt == 0) begin      // t�m veriler g�nderilmi� demektir
            sda_int <= 1;             // sda hatt�n� 1 e �ek  -- ACK bekle
            bit_cnt <= 7;             // 
            state <= SLV_ACK2;
          end else begin
            bit_cnt <= bit_cnt - 1;
            sda_int <= data_tx[bit_cnt-1];      // t�m datalar� teker teker sda int e aktar�yor
            state <= WRITE; 
          end
        end  
        
        READ : begin
          busy <= 1; 
          if(bit_cnt == 0) begin   // okunacak veriler bitti (8 bit)
            if(ena == 1 && addr_rw == {addr,rw} ) begin     // e�er rwde bir de�i�iklik olduysa buraya girmeyecek
              sda_int <= 0;             // ack g�nder -- �oklu veri okumaya devam
            end else begin
              sda_int <= 1;          // nack g�nder
            end
            bit_cnt <= 7;
            data_rd <= data_rx;      // gelen verileri data rd ye ata
            state <= MSTR_ACK;
          end else begin               // 0. byte a gidene kadar cnt yi azalt
            bit_cnt <= bit_cnt - 1;      // asl�nda burada veriler geliyor
            state <= READ;
          end
        end  
                 
        SLV_ACK2 : begin   
        // E�er veri g�nderimin bittiyse STOP state 
        // veri yazmaya devam edeceksen Write state ine
        // veri okuyacaksan Start stateine y�nlendiriyor.
        
          if(ena == 1) begin  // Tek bytel�k yazma i�lemi bitti. Bundan sonra yeniden bir �eyler yazacaksan veya okuma yapacaksan ena ile buray� tetikle
            busy <=0;
            addr_rw <= {addr,rw};    //  rw biti der�i�ebilir ( e�er okuma yapmak istiyorsak)
            data_tx <= data_wr;   
            if(addr_rw == {addr,rw} ) begin    //  burada rw bitinde bir de�i�im var m� diye bak�yor.
            // E�er bir de�i�im varsa, ki bu durum okuma yap�laca�� zaman olur (rw = 1) o zaman START state ine gidiyor. 
            // E�er bir de�i�im yoksa hala yazma yap�lacaksa o zaman �nce en de�erlikli bit g�nderiliyor ve WRITE state ine gidiyor.
              sda_int <= data_wr[bit_cnt];
              state <= WRITE;
            end else begin
              state <= START;
            end
          end else begin
            state <= STOP;
          end
        end                   

        MSTR_ACK : begin  // okuma i�leminden sonra 
          if(ena == 1) begin //d��ar�dan tetik gelmi�se(ena)
            busy <=0;               // tetik gelirse art�k me�gul de�iliz. di�er modullerde ne kadar byte okuyaca��n� buradaki busy ye abakarak ayarla
            addr_rw <= {addr,rw};
            data_tx <= data_wr;
            if(addr_rw == {addr,rw} ) begin
              sda_int <= 1;            //  Anlayamad�m niye High Z -- okumaya devam etmek i�in ack olarak 0 g�ndermemiz gerekmez mi?
              state = READ;
            end else begin         // e�er rw de�i�tiyse yazmak istiyosun veya ba�ka vir slaveden okuma yapmak istiyosun
              state <= START;
            end
          end else begin    // tetik gelmediyse bu kadarl�k veri okuyaca��z
            state <= STOP;        
          end
        end  
        
        
        STOP : begin
          busy  <= 0;
          state <= READY; 
        end
 
        default : begin
          state <= READY;
        end
    
      endcase
      
    end 
    else if ((data_clk == 0) && (data_clk_prev == 1)) begin         // data clk d��en kenarda iken
    
    // 2. state machine  -- ACK ler burada kontrol ediliyor.
      
      case(state)
      
        START : begin
          if(scl_ena == 0) begin  
            scl_ena <= 1;                 // ba�lang��ta scl y� aktif et
            s_ack_error <= 0;
          end
        end      

        SLV_ACK1 : begin
          if((sda != 0) || (s_ack_error == 1)) begin    // slaveden gelen ack i�in sda hatt� 1 ise hata olu�mu� demektir
            s_ack_error <= 1;
          end
        end    
       
        READ : begin
          data_rx[bit_cnt] <= sda;            // READ state inde iken sda hatt�ndaki her bir veriyi data rx e yaz.
        end
      
        SLV_ACK2 : begin
          if((sda != 0) || (s_ack_error == 1)) begin         // SLV ack1 ile ayn� mant�k
            s_ack_error <= 1;
          end
        end               
      
        STOP : begin
        scl_ena <= 0;               //   State STOP ta iken Master olarak scl hatt�n� s�rmeyi b�rak�yoruz. Z de b�rak�ypruz.
        end      
      
        default : begin
        
        end
            
      endcase      
    end
  end
  
  

end
    
    
always @(*) begin  // left tarafta de�i�im oldu�u anda
  if (state == START) begin
    sda_ena_n <= data_clk_prev;             // START state indeyken hat 0 a �ekildi. data clk 1, �nceki de�eri 0;
  end else if (state == STOP) begin
    sda_ena_n <= !data_clk_prev;            // data clk deseydikte olurdu gibi. STOP state tinde iken sda hatt� 1 e �ek.
  end else begin 
    sda_ena_n <= sda_int;         // e�er sda int e 0 atan�rsa sda hatt�n� 0 a �eker. Yoksa sda hatt� 1 de kal�r.
    // sda int teki de�i�imler do�rudan hatta bas�l�yor.
  end 
  o_state <= state;
end


assign scl = (scl_ena == 1 && scl_clk == 0) ? 0 : 1'bZ;   // scl ena 1 ise ayn� zamanda scl clk 0 sa 0 yaz. Aksi t�m durumlarda hatt� high Z de b�rak
assign sda = (sda_ena_n == 0) ? 0 : 1'bZ;                 // sda 

assign ack_error = s_ack_error;



endmodule
